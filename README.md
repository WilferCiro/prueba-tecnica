# Prueba técnica para OLSoftware

**Creador**: Wilfer Daniel Ciro Maya

**Año**: 2021

Previo al clonado del proyecto, realizar la instalación de dependencias, para ello ejecutar el siguiente comando:

### `yarn install`

Para iniciar el proyecto en desarrollo ejecutar

### `yarn start`

Abrir [http://localhost:3000](http://localhost:3000) para ver la página funcional en el navegador.

Se han configurado 3 rutas, las cuales pueden ser accedidas a través de la interacción con la interfáz gráfica:

- http://localhost:3000/login
- http://localhost:3000/panel
- http://localhost:3000/construct

Para generar la versión de producción, ejecutar el comando:

### `yarn build`

el anterior comando crea una versión de producción en la carpeta build del proyecto.

/**
 * Clase para los servicios de consumo de api
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/


import NProgress    from 'nprogress';
import Constant     from '../components/Constants';

class Services {
	static instance;
	static alertModel;
	constructor() {
		if (Services.instance){
			return Services.instance;
		}
		Services.instance              = this;
		this.send                      = this.send.bind(this);
		this.setAlertModel             = this.setAlertModel.bind(this);
	}

	setAlertModel(alertModel) {
		if(alertModel){
			Services.alertModel = alertModel;
		}
	}

	send({
		endpoint=undefined,
		method,
		table="",
		body = {},
		success=undefined,
		error=undefined,
		showMessage=false,
		_headers=true,
		formData=false,
		requires_token=false,
		page=1
	}={}) {
		NProgress.start();
		let xhr = new XMLHttpRequest();
		xhr.responseType = "arraybuffer";

		if(!requires_token){
			body["api_key"] = Constant.get_key();
		}
		endpoint = endpoint + "/" + table;
		if(method==='GET') {
			endpoint = new URL(endpoint);
			endpoint.searchParams.append("body", JSON.stringify(body));
			endpoint.searchParams.append("page", page);
		}
		else if(!formData){
			body = JSON.stringify(body);
		}
		else {
			const data_ = new FormData();
			for(let attribute in body) {
				data_.append(attribute, body[attribute]);
			}
			body = data_;
		}

		if(endpoint !== undefined) {
			console.log(method, endpoint);
			xhr.open(method, endpoint, true);
			if (_headers !== false && !(formData)){
				xhr.setRequestHeader('Content-Type','application/json');
			}
			xhr.onload = function() {
				const decoder = new TextDecoder("UTF-8");
				const buffer = xhr.response;
				let data ={};
				if(buffer !== undefined && buffer !== null && buffer.byteLength > 0) {
					try {
						data = JSON.parse(decoder.decode(buffer));
						data["estado_p"] = xhr.status;
						if(xhr.status !== 200) {
							let message ="";
							if(data.data !== undefined && data.data !== null) {
								for(let error in data.data) {
									message += (message === "") ? error + ": " + data.data[error][0] : ', ' + error + ": " + data.data[error][0];
								}
							}

							if(showMessage && Services.alertModel && Services.alertModel.current){
								if(message !== ''){
									Services.alertModel.current.toggle(true, message, "error");
								}
								else{
									Services.alertModel.current.toggle(true, xhr.status+":"+(( data.data === undefined || data.data.mensaje ===undefined)?Services.messages_requests.get(data.estado_p):data.data.mensaje), "error");
								}
							}
						} // Si existió error
						else if(showMessage && Services.alertModel && Services.alertModel.current){
							Services.alertModel.current.toggle(true,((data.data === undefined  || data.data.mensaje === undefined)?"Acción Exitosa":data.data.mensaje),"success");
						}
						if(success !== undefined && success !== null) {
							success(data);
						}
					}
					catch (err) {
						if(error !== undefined && error !== null){
							error(err);
						}
					}
					finally {
						NProgress.done();
					}
				}
			}
			xhr.send(body);
			xhr.onerror = function(){
				NProgress.done();
			}
		}
	}
}



export default  new Services();

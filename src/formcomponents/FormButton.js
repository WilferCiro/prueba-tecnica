/**
 * Botón de formulario
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/


import React              from 'react';
import BaseFormComponent  from './BaseFormComponent';

class FormButton extends BaseFormComponent{
	constructor(props) {
		super(props);

		this.onClick = this.onClick.bind(this);
		this.additionalFunction = this.props.onClick;
	}

	onClick() {
		if(this.additionalFunction !== null && this.additionalFunction !== undefined) {
			this.additionalFunction();
		}
	}

	render() {
		return (
			<React.Fragment>
				<button
					ref={this.input}
					className={this.props.className}
					onClick={this.onClick}
				>
				{this.props.label}
				</button>
			</React.Fragment>
		);
	}
}

export default FormButton;

/**
 * Componente de formulario de tipo texto
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/


import React              from 'react';
import BaseFormComponent  from './BaseFormComponent';

class FormSelect extends BaseFormComponent{

	constructor(props) {
		super(props);

		this.setOptions = this.setOptions.bind(this);
		this.state = {
			options : this.props.options !== null && this.props.options !== undefined ? this.props.options : []
		}
	}

	setOptions(options) {
		this.setState({
			options: options
		})
	}

	render() {
		return (
			<React.Fragment>
				<label className="label-input-text">{this.props.label}</label>
				<select
					ref={this.input}
					className={this.props.className !== null && this.props.className !== undefined ? this.props.className : "input-text"}
				>
				<option key={Math.random()}></option>
					{
						(this.state.options).map((item, index) => {
							return <option key={Math.random()} value={item.value}>{item.label}</option>
						})
					}
				</select>
			</React.Fragment>
		);
	}
}

export default FormSelect;

/**
 * Componente de formulario de tipo texto
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/


import React              from 'react';
import BaseFormComponent  from './BaseFormComponent';

class FormInputText extends BaseFormComponent{

	render() {
		return (
			<React.Fragment>
				<label className="label-input-text">{this.props.label}</label>
				<input
					ref={this.input}
					className={this.props.className !== null && this.props.className !== undefined ? this.props.className : "input-text"}
					placeholder={this.props.placeholder}
					type="text"
					defaultValue={this.props.defaultValue}
					/>
			</React.Fragment>
		);
	}
}

export default FormInputText;

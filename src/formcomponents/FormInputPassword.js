/**
 * Entrada de contraseña de formulario
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/


import React              from 'react';
import BaseFormComponent  from './BaseFormComponent';
import { FiEye, FiEyeOff } from "react-icons/fi";


class FormInputPassword extends BaseFormComponent{
	constructor(props) {
		super(props);

		this.state = {
			show_password: false
		}

		// Methods
		this.toggleShowPassword = this.toggleShowPassword.bind(this);
	}

	toggleShowPassword() {
		this.setState({
			show_password: !this.state.show_password
		})
	}

	render() {
		return (
			<React.Fragment>
				<label>{this.props.label}</label>
				<div className="input-password">
					<input
						ref={this.input}
						className={this.props.className}
						placeholder={this.props.placeholder}
						type={this.state.show_password ? "text" : "password"}
						defaultValue={this.props.defaultValue}
						/>
					<button onClick={this.toggleShowPassword}>{this.state.show_password ? <FiEyeOff /> : <FiEye />}</button>
				</div>
			</React.Fragment>
		);
	}
}

export default FormInputPassword;

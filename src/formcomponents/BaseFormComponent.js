/**
 * Clase de donde heredan los componentes de formulario
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React          from 'react';

class BaseFormComponent extends React.Component{
	constructor(props) {
		super(props);

		// Variables
		this.input            = React.createRef();

		this.required = this.props.required !== null && this.props.required !== undefined ? this.props.required :

		// Methods
		this.getValue         = this.getValue.bind(this);
		this.setValue         = this.setValue.bind(this);
		this.valid            = this.valid.bind(this);
	}

	getValue() {
		return this.input.current.value;
	}
	setValue(value) {
		this.input.current.value = value;
	}

	valid() {
		if(this.getValue() === "" && this.required === true){
			//this.errores.
			return false;
		}
		return true;
	}
}

export default BaseFormComponent;

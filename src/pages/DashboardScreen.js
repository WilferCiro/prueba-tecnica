/**
 * Componente encargado de mostrar la pantalla de dashboard al usuario final
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import DashboardMenu       from '../components/DashboardMenu';
import DashboardHeader     from '../components/DashboardHeader';
import DashboardContent    from '../containers/DashboardContent';
import DashboardFooter     from '../components/DashboardFooter';

// Screen
import DashboardUsuarios     from './screens/DashboardUsuarios';

class DashboardScreen extends BasePanel{
	constructor(props) {
		super(props);

		// Methods
		this.toggleDashboardMenu = this.toggleDashboardMenu.bind(this);
	}

	componentDidMount() {
		let classNameDashboard = BasePanel.localStorage.getItem("classDashboard");
		if(classNameDashboard !== null && classNameDashboard !== undefined && classNameDashboard !== "") {
			this.refs["dashboard"].classList.add(classNameDashboard);
		}
	}


	toggleDashboardMenu(){
		this.refs["dashboard"].classList.toggle("dashboard-open-menu");

		let classNameDashboard = BasePanel.localStorage.getItem("classDashboard");
		BasePanel.localStorage.setItem("classDashboard", classNameDashboard === "" || classNameDashboard === null || classNameDashboard === undefined ? "dashboard-open-menu" : "");
		//BasePanel.classDashboard = BasePanel.classDashboard !== "" ? "dashboard-open-menu" : "";
	}

	render() {
		return (
			<div className={"dashboard"} ref="dashboard">
				<DashboardMenu
					toggleDashboardMenu={this.toggleDashboardMenu}
				/>

				<div className="dashboard-content">

					<DashboardHeader
						toggleDashboardMenu={this.toggleDashboardMenu}
						/>

					<DashboardContent>
						<DashboardUsuarios />
						<DashboardFooter></DashboardFooter>
					</DashboardContent>

				</div>
			</div>
		);
	}
}

export default DashboardScreen;

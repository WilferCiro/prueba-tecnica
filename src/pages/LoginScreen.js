/**
 * Componente encargado de mostrar la pantalla de login al usuario final
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import Login              from '../containers/Login';

class LoginScreen extends BasePanel{

	render() {
		return (
			<React.Fragment>
				<Login />
			</React.Fragment>
		);
	}
}

export default LoginScreen;

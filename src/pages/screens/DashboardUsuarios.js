/**
 * Componente encargado de mostrar la vista de tabla usuarios
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../../containers/BasePanel';
import {FiUsers}          from "react-icons/fi";

// Form
import FormSelect         from '../../formcomponents/FormSelect';
import FormInputText      from '../../formcomponents/FormInputText';
import Table              from '../../components/Table';
import Modal              from '../../components/Modal';

let tableHeader = [
	{"item" : "nombres", "label" : "Nombres"},
	{"item" : "apellidos", "label" : "Apellidos"},
	{"item" : "identificacion", "label" : "Identificación (C.C)"},
	{"item" : "rol_asociado", "label" : "Rol Asociado"},
	{"item" : "estado", "label" : "Estado"},
	{"item" : "telefono", "label" : "Teléfono"},
	{"item" : "correo_electronico", "label" : "Correo electrónico"},
]

class DashboardUsuarios extends BasePanel{
	constructor(props) {
		super(props);

		// Methods
		this.startEdition = this.startEdition.bind(this);
		this.startDelete = this.startDelete.bind(this);
		this.startCreation = this.startCreation.bind(this);
		this.clearFilters = this.clearFilters.bind(this);
		this.filter     = this.filter.bind(this);
		this.acceptModal = this.acceptModal.bind(this);
		this.successEditAdd = this.successEditAdd.bind(this);
		this.successStatuses = this.successStatuses.bind(this);
		this.successRoles = this.successRoles.bind(this);

		//References
		this.filterNamesRef = React.createRef();
		this.filterLastnamesRef = React.createRef();
		this.filterCCRef = React.createRef();
		this.filterRolRef = React.createRef();
		this.filterPassRef = React.createRef();
		this.filterPhoneRef = React.createRef();
		this.filterEmailRef = React.createRef();
		this.filterStatusRef = React.createRef();

		this.formNamesRef = React.createRef();
		this.formLastnamesRef = React.createRef();
		this.formCCRef = React.createRef();
		this.formRolRef = React.createRef();
		this.formPassRef = React.createRef();
		this.formPhoneRef = React.createRef();
		this.formEmailRef = React.createRef();
		this.formStatusRef = React.createRef();

		// Attributes
		this.modal = React.createRef();
		this.modalEliminar = React.createRef();
		this.tableRef = React.createRef();
	}

	componentDidMount() {

		this.send({
			endpoint: this.constants.getPublicEndpoint(),
			table: 'rol_asociado',
			method: 'GET',
			body: {
				"modelo" : "todo"
			},
			success: this.successRoles
		});

		this.send({
			endpoint: this.constants.getPublicEndpoint(),
			table: 'estado',
			method: 'GET',
			body: {
				"modelo" : "todo"
			},
			success: this.successStatuses
		});
	}

	successRoles(data) {
		if(data["estado_p"] === 200) {
			let newData = [];
			for (let indexData in data["data"]) {
				newData.push({
					"value" : data["data"][indexData]["pk"],
					"label" : data["data"][indexData]["titulo"]
				});
			}
			this.formRolRef.current.setOptions(newData);
			this.filterRolRef.current.setOptions(newData);
		}
	}

	successStatuses(data) {
		if(data["estado_p"] === 200) {
			let newData = [];
			for (let indexData in data["data"]) {
				newData.push({
					"value" : data["data"][indexData]["pk"],
					"label" : data["data"][indexData]["titulo"]
				});
			}
			this.formStatusRef.current.setOptions(newData);
			this.filterStatusRef.current.setOptions(newData);
		}
	}

	startDelete(data) {
		this.modalEliminar.current.open();
	}
	startEdition(data) {
		this.modal.current.open();

		this.formNamesRef.current.setValue(data.nombres);
		this.formLastnamesRef.current.setValue(data.apellidos);
		this.formCCRef.current.setValue(data.identificacion);
		this.formRolRef.current.setValue(data.rol_asociado__pk);
		this.formStatusRef.current.setValue(data.estado__pk);
		this.formPassRef.current.setValue("");
		this.formPhoneRef.current.setValue(data.telefono);
		this.formEmailRef.current.setValue(data.correo_electronico);
	}
	startCreation() {
		this.modal.current.open();
		this.formNamesRef.current.setValue("");
		this.formLastnamesRef.current.setValue("");
		this.formCCRef.current.setValue("");
		this.formRolRef.current.setValue("");
		this.formStatusRef.current.setValue("");
		this.formPassRef.current.setValue("");
		this.formPhoneRef.current.setValue("");
		this.formEmailRef.current.setValue("");
	}

	acceptModal() {
		let body = {};
		body["nombres"] = this.formNamesRef.current.getValue();
		body["apellidos"] = this.formLastnamesRef.current.getValue();
		body["identificacion"] = this.formCCRef.current.getValue();
		body["rol_asociado"] = this.formRolRef.current.getValue();
		body["estado"] = this.formStatusRef.current.getValue();
		body["telefono"] = this.formPhoneRef.current.getValue();
		body["correo_electronico"] = this.formEmailRef.current.getValue();
		body["modelo"] = "crear";
		this.send({
			endpoint: this.constants.getPublicEndpoint(),
			table: 'usuario',
			method: 'POST',
			body: body,
			success: this.successEditAdd
		});
	}
	successEditAdd(data) {
		if(data["estado_p"] === 200) {
			this.modal.current.close();
			this.tableRef.current.reload();
		}
	}

	clearFilters(){
		this.filterNamesRef.current.setValue("");
		this.filterLastnamesRef.current.setValue("");
		this.filterCCRef.current.setValue("");
		this.filterRolRef.current.setValue("");
		this.filterPassRef.current.setValue("");
		this.filterPhoneRef.current.setValue("");
		this.filterEmailRef.current.setValue("");
		this.filterStatusRef.current.setValue("");
		this.filter();
	}

	filter(){
		let filtros = {};
		filtros["nombres-contiene"] = this.filterNamesRef.current.getValue();
		filtros["apellidos-contiene"] = this.filterLastnamesRef.current.getValue();
		filtros["identificacion-contiene"] = this.filterCCRef.current.getValue();
		filtros["rol_asociado"] = this.filterRolRef.current.getValue();
		filtros["estado"] = this.filterStatusRef.current.getValue();
		filtros["telefono-contiene"] = this.filterPhoneRef.current.getValue();
		filtros["correo_electronico-contiene"] = this.filterEmailRef.current.getValue();
		for(let key in filtros){
			if(filtros[key] === "") {
				delete filtros[key];
			}
		}
		this.tableRef.current.setFiltros(filtros);
	}

	acceptEliminar() {
		this.modalEliminar.current.close();
	}

	render() {
		return (
			<div>
				<Modal ref={this.modalEliminar} title="¿Eliminar usuario?" size="1">
					<h3>¿Está seguro de eliminar este usuario?</h3>
					<div className="separator-vertical" />
					<div className="btn-container-2">
						<button className="btn-secundary" onClick={(e) => this.acceptEliminar()}>Eliminar</button>
					</div>
				</Modal>
				<Modal ref={this.modal} title="Formulario de usuarios">
					<div className="form-divided-2">
						<div>
							<FormInputText
								ref={this.formNamesRef}
								label="Nombres"
							/>
							<div className="separator-vertical" />
							<FormInputText
								ref={this.formCCRef}
								label="Identificación (C.C)"
							/>
							<div className="separator-vertical" />
								<FormSelect
									ref={this.formStatusRef}
									label="Estado"
								/>
							<div className="separator-vertical" />
							<FormInputText
								ref={this.formPhoneRef}
								label="Teléfono"
							/>
							<div className="separator-vertical" />
						</div>
						<div>
							<FormInputText
								ref={this.formLastnamesRef}
								label="Apellidos"
							/>
							<div className="separator-vertical" />
							{/*<FormInputText
								ref={this.formRolRef}
								label="Rol asociado"
							/>*/}
							<FormSelect
								ref={this.formRolRef}
								label="Rol asociado"
							/>
							<div className="separator-vertical" />
							<FormInputText
								ref={this.formPassRef}
								label="Contraseña"
							/>
							<div className="separator-vertical" />
							<FormInputText
								ref={this.formEmailRef}
								label="Correo electrónico"
							/>
							<div className="separator-vertical" />

						</div>

						<div className="btn-container-2">
							<button className="btn-secundary" onClick={(e) => this.acceptModal()}>Aceptar</button>
						</div>
					</div>

				</Modal>
				<div className="tabla-view">
					<section>
						<div className="section-title">
							<div className="section-title-icon">
								<FiUsers />
							</div>
							<div>
								Usuarios existentes
							</div>
							<div>
								<button className="btn-primary" onClick={this.startCreation}>Crear</button>
							</div>
						</div>

						<Table
							ref={this.tableRef}
							headers={tableHeader}
							onEdit={this.startEdition}
							onDelete={this.startDelete}
							service={{
								"table" : "usuario",
								"model" : "todo"
							}}
						/>
					</section>

					<section>
						<div className="section-title">
							<div className="section-title-icon">
								<FiUsers />
							</div>
							<div>
								Filtrar búsqueda
							</div>
						</div>

						<FormInputText
							ref={this.filterNamesRef}
							label="Nombres"
						/>
						<div className="separator-vertical" />
						<FormInputText
							ref={this.filterLastnamesRef}
							label="Apellidos"
						/>
						<div className="separator-vertical" />
						<FormInputText
							ref={this.filterCCRef}
							label="Identificación (C.C)"
						/>
						<div className="separator-vertical" />
						<FormSelect
							ref={this.filterRolRef}
							label="Rol asociado"
						/>
						<div className="separator-vertical" />
						<FormSelect
							ref={this.filterStatusRef}
							label="Estado"
						/>
						<div className="separator-vertical" />
						<FormInputText
							ref={this.filterPassRef}
							label="Contraseña"
						/>
						<div className="separator-vertical" />
						<FormInputText
							ref={this.filterPhoneRef}
							label="Teléfono"
						/>
						<div className="separator-vertical" />
						<FormInputText
							ref={this.filterEmailRef}
							label="Correo electrónico"
						/>
						<div className="separator-vertical" />
						<div className="btn-container-2">
							<button className="btn-secundary" onClick={this.filter}>Filtrar</button>
							<button className="btn-clean" onClick={this.clearFilters}>Limpiar</button>
						</div>

					</section>
				</div>
			</div>
		);
	}
}

export default DashboardUsuarios;

/**
 * Componente encargado de mostrar la pantalla de "bajo construcción"
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../../containers/BasePanel';
import {FiTool}          from "react-icons/fi";

class UnderConstruction extends BasePanel{

	render() {
		return (
			<div>
				<section className="construction">
					<div className="section-title">
						<div className="section-title-icon">
							<FiTool />
						</div>
						<div>
							Sitio en construcción
						</div>
					</div>

					<h2>Este sitio está en plena construcción</h2>
				</section>
			</div>
		);
	}
}

export default UnderConstruction;

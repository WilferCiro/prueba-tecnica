/**
 * Clase padre de donde heredan los demás componentes
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React, {
	Component
} from 'react';

import Constant     from '../components/Constants';
import Services     from '../utils/Services';

export default class BasePanel extends Component {
	static history = undefined;
	static _constant = Constant;
	static urls_servidores = undefined;
	static service = Services;
	static alertLocal = new React.createRef();

	static screenLoading = new React.createRef();
	static localStorage = window.localStorage;

	constructor(props) {
		super(props);
		this.constants = BasePanel._constant;

		this.redirectPage = this.redirectPage.bind(this);
		this.goHome = this.goHome.bind(this);
		this.URLSave = null;

		this.send = this.send.bind(this);
		this.error = this.error.bind(this);

		if (this.props.history !== null && this.props.history !== undefined && BasePanel.history === undefined){
			BasePanel.history = this.props.history;
		}

	}


	error(data) {
		console.log("--ERRR----", data);
	}

	redirectPage(to) {
		BasePanel.history.replace(to);
	}

	goHome() {
		this.redirectPage(this.constants.route_index, this.constants.route_index_alias);
	}

	send(data) {
		BasePanel.service.setAlertModel(BasePanel.alertDavinci);
		BasePanel.service.send(data);
	}

}
//export default BasePanel;

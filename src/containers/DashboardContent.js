/**
 * Componente encargado de mostrar el contenido del cuerpo del dashboard
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';

class DashboardContent extends BasePanel{
	render() {
		return (
			<div className="dashboard-body">
				{this.props.children}
			</div>
		);
	}
}

export default DashboardContent;

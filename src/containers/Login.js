/**
 * Componente encargado de mostrar la pantalla de login al usuario final
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import FormInputText      from '../formcomponents/FormInputText';
import FormInputPassword  from '../formcomponents/FormInputPassword';
import FormButton         from '../formcomponents/FormButton';

class Login extends BasePanel{
	constructor(props) {
		super(props);

		// objects
		this.usernameRef = React.createRef();
		this.passwordRef = React.createRef();

		// Methods
		this.onClickLogin = this.onClickLogin.bind(this);
		this.successLogin = this.successLogin.bind(this);
	}

	onClickLogin(){
		let username = this.usernameRef.current.getValue();
		let password = this.passwordRef.current.getValue();
		if(BasePanel.screenLoading.current !== null && BasePanel.screenLoading.current !== undefined) {
			BasePanel.screenLoading.current.showLoading(true);
		}

		let body = {
			"username" : username,
			"password" : password
		}
		this.redirectPage("panel");
		this.send({
			endpoint: this.constants.getPublicEndpoint(),
			service: "producto",
			method: 'POST',
			body: body,
			success: this.successLogin
		});
	}

	successLogin(data) {
		if(BasePanel.screenLoading.current !== null && BasePanel.screenLoading.current !== undefined) {
			BasePanel.screenLoading.current.showLoading(false);
		}
	}

	render() {
		return (
			<div className="login-page">

				<div className="login-background"></div>
				<div className="login-sketch">
					<div className="login-title">
						<h1>
							Aplicación<br />
							OLSoftware
						</h1>
						<h3>Prueba práctica Front-end senior</h3>
					</div>

					<div className="login-form">
						<h4>Inicio de sesión</h4>
						<FormInputText
							ref={this.usernameRef}
							placeholder="Usuario"
							className="input-login"
						/>
						<FormInputPassword
							ref={this.passwordRef}
							placeholder="Contraseña"
							className="input-login"
						/>
						<FormButton
							className="button-login"
							label="Iniciar sesión"
							onClick={this.onClickLogin}
							/>
					</div>
				</div>

				<footer className="footer-login">
					OLSoftware - 2021
				</footer>
			</div>
		);
	}
}

export default Login;

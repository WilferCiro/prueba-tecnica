/**
 * Clase para manejar las constantes de la aplicación
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

class Constant{
	static instance = null;
	// general
	static URL_public = "/api/public";
	static URL_private = "/api/private";

	// Dev
	static URL_server = "http://kiwipyme.pythonanywhere.com";


	constructor(){
		if (Constant.instance!==null){
			return Constant.instance;
		}
		Constant.instance = this;
	}

	getServer() {
		return Constant.URL_server;
	}
	getPublicEndpoint() {
		return Constant.URL_server + Constant.URL_public;
	}
	getPrivateEndpoint() {
		return Constant.URL_server + Constant.URL_private;
	}

	get_key() {
		return "123";
	}


}

export default  new Constant();

/**
 * Componente encargado de mostrar el header del dashboard
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import {FiMenu, FiLogOut, FiUser}          from "react-icons/fi";

class DashboardHeader extends BasePanel{

	render() {
		return (
			<header>
				<div className="center-vertical">
					<button className="header-button" onClick={this.props.toggleDashboardMenu}><FiMenu /></button>
				</div>
				<div className="center-vertical">
					<h2>Prueba Front-end</h2>
				</div>
				<div className="header-user">
					<div className="header-user-icon center-vertical">
						<FiUser />
					</div>
					<div className="header-user-name center-vertical">
						Wilfer Daniel Ciro Maya
					</div>
				</div>
				<div>
					<button className="header-button" onClick={(e) => {this.redirectPage("login")}}><FiLogOut /></button>
				</div>
			</header>
		);
	}
}

export default DashboardHeader;

/**
 * Componente encargado de mostrar la tabla al usuario final
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import {FiEdit2, FiTrash2, FiChevronsLeft, FiChevronLeft, FiChevronRight, FiChevronsRight}           from "react-icons/fi";

class Table extends BasePanel{
	constructor(props) {
		super(props);

		this.headers = this.props.headers !== null && this.props.headers !== undefined ? this.props.headers : [];
		this.onEdit = this.props.onEdit;
		this.onDelete = this.props.onDelete;
		this.serviceGET = this.props.service;

		// Methods
		this.goTo = this.goTo.bind(this);
		this.reload = this.reload.bind(this);
		this.successReload = this.successReload.bind(this);
		this.setFiltros = this.setFiltros.bind(this);
		this.filtros = {}

		this.page = 1;

		// States
		this.state = {
			registers : [],
			paginator: {
				current : 1,
				count : 1,
				num_pages: 1
			}
		}
	}

	componentDidMount() {
		if(this.serviceGET !== null && this.serviceGET !== undefined) {
			this.reload();
		}
	}

	setFiltros(filtros) {
		this.filtros = filtros;
		this.reload();
	}

	reload() {
		if(BasePanel.screenLoading.current !== null && BasePanel.screenLoading.current !== undefined) {
			BasePanel.screenLoading.current.showLoading(true);
		}
		this.send({
			endpoint: this.constants.getPublicEndpoint(),
			table: this.serviceGET.table,
			method: 'GET',
			body: {
				"modelo" : this.serviceGET.model,
				"paginate" : true,
				"cantidad" : 5,
				"campos" : this.filtros
			},
			page: this.page,
			success: this.successReload
		})
	}

	successReload(data) {
		if(data["estado_p"] === 200){
			this.setState({
				registers : data["data"],
				paginator : data["paginator"]
			});
		}
		else{
			this.setState({
				registers : [],
				paginator : {
					current : 1,
					count : 1,
					num_pages: 1
				}
			});
		}
		if(BasePanel.screenLoading.current !== null && BasePanel.screenLoading.current !== undefined) {
			BasePanel.screenLoading.current.showLoading(false);
		}
	}

	goTo(page) {
		this.page = page;
		this.reload();
	}

	render() {
		let tableBody = <tbody>
			{
				(this.state.registers).map((register, index) => {
					return <tr key={Math.random()}>
						{
							(this.headers).map((header, index) => {
								return <td key={Math.random()}>{register[header.item]}</td>
							})
						}
						<td>
							{
								(this.onEdit !== undefined && this.onEdit !== null) ?
									<button className="table-btn btn-edit" onClick={(e) => this.onEdit(register)}><FiEdit2 /></button>
								:
								null
							}
							{
								(this.onDelete !== undefined && this.onDelete !== null) ?
									<button className="table-btn btn-delete" onClick={(e) => this.onDelete(register)}><FiTrash2 /></button>
								:
								null
							}
						</td>
					</tr>
				})
			}
		</tbody>

		let tableHeader = <thead>
			<tr>
				{
					(this.headers).map((item, index) => {
						return <th key={Math.random()}>{item.label}</th>
					})
				}
				{
					((this.onEdit !== undefined && this.onEdit !== null) || (this.onDelete !== undefined && this.onDelete !== null)) ?
						<th>Acción</th>
					:
					null
				}
			</tr>
		</thead>


		return (
			<div>
				<div className="table-container">
					<table>
						{tableHeader}
						{tableBody}
					</table>
					Mostrando: {this.state.registers.length} de {this.state.paginator.count}
				</div>
				<div className="table-paginator">
					<ul className="paginador">
						{
							(this.state.paginator.current > 1) ?
								<React.Fragment>
									<li onClick={(e) => this.goTo(1)}><FiChevronsLeft /></li>
									<li onClick={(e) => this.goTo(this.state.paginator.current - 1)}><FiChevronLeft /></li>
								</React.Fragment>
							:
							null
						}
						{
							(Array.from(Array(this.state.paginator.num_pages).keys())).map((item, index) => {
								return <li className={this.state.paginator.current === item+1 ? "paginador-selected" : ""} key={Math.random()} onClick={(e) => this.goTo(item + 1)}>{item + 1}</li>
							})
						}
						{
							(this.state.paginator.current + 1 <= this.state.paginator.num_pages) ?
								<React.Fragment>
									<li onClick={(e) => this.goTo(this.state.paginator.current + 1)}><FiChevronRight /></li>
									<li onClick={(e) => this.goTo(this.state.paginator.num_pages)}><FiChevronsRight /></li>
								</React.Fragment>
							:
							null
						}
					</ul>
				</div>
				<div className="clear-both" />
			</div>
		);
	}
}

export default Table;

/**
 * Componente encargado de mostrar el footer del dashboard
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';

class DashboardFooter extends BasePanel{

	render() {
		return (
			<footer className="dashboard-footer">
				OLSoftware - 2021
			</footer>
		);
	}
}

export default DashboardFooter;

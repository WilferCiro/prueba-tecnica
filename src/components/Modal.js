/**
 * Componente modal de la aplicación
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React          from 'react';
import BasePanel      from '../containers/BasePanel';

class Modal extends BasePanel{
	constructor(props) {
		super(props);
		this.state = {
			open : false
		}
	}
	componentDidMount() {
	}

	close() {
		this.setState({
			open: false
		})
	}

	open() {
		this.setState({
			open: true
		})
	}

	render() {
		let classbg = this.state.open ? "open" : "";
		let sizeModal = this.props.size !== null && this.props.size !== undefined ? this.props.size : "2";
		return (
			<div>
				<div className={"modal-background " + classbg} onClick={(e) => this.close()}></div>
				<div className={"modal modal-" + sizeModal + " " + classbg}>
					<div className="modal-header">
						<div>{this.props.title}</div>
						<div><button className="modal-btn-close" onClick={(e) => this.close()}>x</button></div>
					</div>
					<div className="modal-body">
						{this.props.children}
					</div>
					<div className="modal-footer">

					</div>
				</div>
			</div>
		);
	}
}

export default Modal;

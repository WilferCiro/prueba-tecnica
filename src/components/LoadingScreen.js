/**
 * Componente encargado de mostrar la pantalla de carga de la aplicación
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';

class LoadingScreen extends BasePanel{
	constructor(props) {
		super(props);

		// states
		this.state = {
			show : false
		}

		// Methods
		this.showLoading = this.showLoading.bind(this);
	}

	showLoading(status) {
		this.setState({
			show: status
		})
	}


	render() {
		let className = "loading-screen";
		if(this.state.show) {
			className += " loading-screen-show";
		}
		return (
			<div className={className}>
				<div className="loading-screen-text">
					<h2>Estamos preparando todo para tí</h2>
					<div className="dot-elastic"></div>
				</div>
			</div>
		);
	}
}

export default LoadingScreen;

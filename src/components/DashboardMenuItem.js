/**
 * Componente encargado de mostrar el item del menú del dashboard
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import {FiChevronDown, FiChevronUp}          from "react-icons/fi";

class DashboardMenuItem extends BasePanel{
	constructor(props) {
		super(props);

		this.items = this.props.items !== null && this.props.items !== undefined ? this.props.items : [];

		this.state = {
			open : false
		}

		this.toggleSubItems = this.toggleSubItems.bind(this);
	}

	toggleSubItems() {
		if(this.items.length > 0){
			this.setState({
				open: !this.state.open
			});
		}
	}

	render() {
		let className = "dasboard-menu-item";
		let iconArrow;
		let internalClass = "";
		if(this.props.onClick !== undefined && this.props.onClick !== null) {
			className += " dasboard-menu-item-hover"
		}
		if(this.items.length > 0){
			iconArrow = <FiChevronUp />
			internalClass = "dasboard-menu-item-internal-open";
			if(!this.state.open){
				internalClass = "";
				iconArrow = <FiChevronDown />
			}
		}
		return (
			<div>
				<div className={className} onClick={this.toggleSubItems}>
					<div className="dasboard-menu-item-icon center-vertical">
						{this.props.icon}
					</div>

					<div className="dasboard-menu-item-text">
						<p>{this.props.text}</p>
					</div>
					<div className="dasboard-menu-item-row">
						{iconArrow}
					</div>
				</div>
				{
					(this.items).map((item, index) => {
						return <div className={internalClass + " dasboard-menu-item-internal dasboard-menu-item dasboard-menu-item-hover"} key={Math.random()} onClick={(e) => this.props.onClick(item.key)}>
							<div className="dasboard-menu-item-icon center-vertical">
								{item.icon}
							</div>
							<div className="dasboard-menu-item-text">
								<p>{item.label}</p>
							</div>
						</div>
					})
				}

			</div>
		);
	}
}

export default DashboardMenuItem;

/**
 * Componente encargado de mostrar el menú del dashboard
 *
 * @creator: Wilfer Daniel Ciro Maya
 * @year: 2021
**/

import React              from 'react';
import BasePanel          from '../containers/BasePanel';
import DashboardMenuItem   from './DashboardMenuItem';
import {FiBox, FiLayers, FiCalendar, FiUser, FiLock, FiUserCheck, FiPrinter}          from "react-icons/fi";

class DashboardMenu extends BasePanel{
	constructor(props) {
		super(props);

		// Methods
		this.onClickMenu = this.onClickMenu.bind(this)

		// Attributes
	}

	onClickMenu(redirect) {
		this.redirectPage(redirect);
	}

	render() {
		return (
			<nav className="dashboard-menu dashboard-menu-max">
				<button className="close-menu-nav-mobile" onClick={this.props.toggleDashboardMenu}>X</button>
				<DashboardMenuItem
					text="OLSoftware"
					icon={<FiBox/>}
				/>
				<hr />

				<DashboardMenuItem
					text="Programación"
					onClick={this.onClickMenu}
					icon={<FiLayers/>}
					items={[{
						"key" : "panel",
						"label" : "Programación",
						"icon" : <FiLayers />
					}]}
				/>
				<DashboardMenuItem
					text="Gestión de operaciones"
					onClick={this.onClickMenu}
					icon={<FiCalendar/>}
					items={[{
						"key" : "construct",
						"label" : "Gestión de operaciones 2",
						"icon" : <FiCalendar />
					}]}
				/>
				<DashboardMenuItem
					text="Perfiles"
					onClick={this.onClickMenu}
					icon={<FiUser/>}
					items={[{
						"key" : "panel",
						"label" : "Roles",
						"icon" : <FiLock />
					},
					{
						"key" : "panel",
						"label" : "Usuarios",
						"icon" : <FiUserCheck />
					}
					]}
				/>
				<DashboardMenuItem
					text="Reportes"
					onClick={this.onClickMenu}
					icon={<FiPrinter/>}
				/>
			</nav>
		);
	}
}

export default DashboardMenu;

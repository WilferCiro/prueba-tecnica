import React              from 'react';
import ReactDOM           from 'react-dom';
import reportWebVitals    from './reportWebVitals';
import LoadingScreen      from './components/LoadingScreen';
import BasePanel          from './containers/BasePanel';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { createBrowserHistory } from "history";
// Pages
import LoginScreen from './pages/LoginScreen';
import DashboardScreen from './pages/DashboardScreen';
import DashboardConstruction from './pages/DashboardConstruction';

// Styles
import  './css/index.css';
import  './css/responsive.css';

// Import pages

const history = createBrowserHistory();

ReactDOM.render(
	<div>
		<Router>
			<Switch>
				<Route history={history} path='/login' component={LoginScreen} />
				<Route history={history} path='/construct' component={DashboardConstruction} />
				<Route history={history} path='/panel' component={DashboardScreen} />
				<Redirect to="/login" />
			</Switch>
		</Router>

		<LoadingScreen
			ref={BasePanel.screenLoading}
			/>
	</div>,
	document.getElementById('root')
);

reportWebVitals();
